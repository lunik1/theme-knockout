#+title: theme-knockout.el

Helps you find your favourite theme by running a single-elimination tournament
of available themes.

To start, run ~theme-knockout~[fn:1] and navigate the hydra with
- ←/→ :: to view the other theme in the current faceoff
- ↓ :: to choose the current theme as the winner of the current faceoff
- ↑ :: to undo your previous choice
- q :: to quit and restore the original theme
- Q :: to quit and keep the current theme

The winning theme, once arrived at, will be displayed as a message.

By default, the themes given by ~(custom-available-themes)~ are used in the
tournament, to use a custom list set ~theme-knockout-competitors~.

To show the currently active theme name in the hydra, set
~theme-knockout-show-theme-name~ to ~t~.

[fn:1] note that during the tournament, Emacs will not prompt you to load
potentially unsafe themes.
