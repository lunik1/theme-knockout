;;; theme-knockout.el --- Find your preferred theme with an elimination tournament -*- lexical-binding: t -*-

;; Copyright (C) 2023 lunik1

;; Author: lunik1 <https://gitlab.com/lunik1>
;; Homepage: https://gitlab.com/lunik1/theme-knockout
;; Version: 1.0.0
;; Keywords: faces
;; Package-Requires: ((emacs "28.2") (hydra "0.15.0") (queue "0.2"))

;; This file is not part of GNU Emacs.

;;; License:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation version 3.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Package to help find a favourite theme via a single-elimination tournament.

;;; Code:

(require 'cl-lib)
(require 'hydra)
(require 'queue)

(cl-defstruct
    (theme-knockout-tournament
     (:constructor theme-knockout--make-tournament))
  "Structure representing a single elimination tournament.

Slots:

`matches'
    A QUEUE of cons cells, where each cons cell represents a matchup between
    that cons cell's car and cdr. The head of the queue represents the current
    match. Neither car nor cdr of these cells may be nil.

`unmatched'
    A competitor currently waiting for an opponent, nil if there is no such
    competitor. Will be non-nill whenever there are an odd number of competitors
    remaining in the tournament.

`match-log'
    History of completed matches as a list of cons cells. The car of each cell is
    the victor of that match."
  (matches (make-queue) :type queue)
  unmatched
  match-log)

(defun theme-knockout--shuffle (list)
  "Return a shuffled version of LIST. LIST itself is unchanged."
  (setf list (cl-copy-list list))
  (cl-loop for i downfrom (1- (length list)) to 0
           do (cl-rotatef (elt list i) (elt list (random (1+ i)))))
  list)

(defun theme-knockout--make-match-queue (competitors)
  "Transform COMPETITORS into matches.
Takes the list of COMPETITORS for a tournament and return a queue of cons
pairs, representing matchups.

Will throw an error if there are not an even number of competitors."
  (cl-check-type competitors proper-list)
  (cl-assert (cl-evenp (length competitors)))
  (let ((match-queue (make-queue)))
    (while (not (null competitors))
      (queue-enqueue match-queue (cons (pop competitors) (pop competitors))))
    match-queue))

(defun theme-knockout--tournament-from-competitiors (competitors)
  "Create a tournament structure from a list of COMPETITORS.

The matchups will be random."
  (cl-check-type competitors (and proper-list (not null)))
  (let* ((competitors (theme-knockout--shuffle competitors))
         (unmatched (when (cl-oddp (length competitors)) (pop competitors))))
    (theme-knockout--make-tournament
     :matches (theme-knockout--make-match-queue competitors)
     :unmatched unmatched
     :match-log (list))))

(defun theme-knockout--tournament-next-match (tournament match-winner)
  "Declare MATCH-WINNER as the winner of the current match.
Advance TOURNAMENT to the next match and return the advanced TOURNAMENT."
  (cl-check-type tournament theme-knockout-tournament)
  (let ((current-match (queue-first (theme-knockout-tournament-matches
                                     tournament))))
    (cond
     ((equal match-winner (car current-match))
      (setf (theme-knockout-tournament-match-log tournament)
            (cons current-match
                  (theme-knockout-tournament-match-log tournament))))
     ((equal match-winner (cdr current-match))
      (setf (theme-knockout-tournament-match-log tournament)
            (cons (cons (cdr current-match) (car current-match))
                  (theme-knockout-tournament-match-log tournament))))
     (t
      (error "%s" "winning theme not in match"))))
  (queue-dequeue (theme-knockout-tournament-matches tournament))
  (if-let ((unmatched (theme-knockout-tournament-unmatched tournament)))
      (progn
        (setf (theme-knockout-tournament-unmatched tournament) nil)
        (queue-enqueue (theme-knockout-tournament-matches tournament)
                       (cons unmatched match-winner)))
    (setf (theme-knockout-tournament-unmatched tournament) match-winner))
  tournament)

(defun theme-knockout--queue-dequeue-end (queue)
  "Remove the last element of QUEUE and return it.
Returns nil if the queue is empty."
  ;; TODO Should be possible in 1 pass
  (prog1
      (queue-tail queue)
    (setf (queue-head queue) (butlast (queue-head queue))
          (queue-tail queue) (last (queue-head queue)))))


(defun theme-knockout--tournament-undo-match (tournament)
  "Undo the previous match in TOURNAMENT.

Restore the TOURNAMENT as if the most recently completed match did not occur. If
no matches have taken place, do nothing.

The car and cdr of the undone match may be swapped from their original
positions."
  (cl-check-type tournament theme-knockout-tournament)
  (unless (null (theme-knockout-tournament-match-log tournament))
    (let ((previous-match (pop (theme-knockout-tournament-match-log
                                tournament))))
      (queue-prepend (theme-knockout-tournament-matches tournament)
                     previous-match)
      (if (null (theme-knockout-tournament-unmatched tournament))
          (progn (setf (theme-knockout-tournament-unmatched tournament)
                       (car previous-match))
                 (theme-knockout--queue-dequeue-end
                  (theme-knockout-tournament-matches tournament)))
        (setf (theme-knockout-tournament-unmatched tournament) nil)))))

(defun theme-knockout--tournament-winnerp (tournament)
  "Return the winner of TOURNAMENT if one exists, else nil."
  (cl-check-type tournament theme-knockout-tournament)
  (when (queue-empty (theme-knockout-tournament-matches tournament))
    (theme-knockout-tournament-unmatched tournament)))

(defmacro theme-knockout--color-block (color)
  "Create a string that will display as a block of ANSI-term COLOR."
  `(let ((str (string ?█ ?█ ?█)))
     (set-text-properties 0 3
                          '(face ,(intern (format "ansi-color-%s" color)))
                          str)
     str))

;; State

(defvar theme-knockout--theme-tournament nil)
(defvar theme-knockout--original-theme (car custom-enabled-themes))
(defvar theme-knockout--custom-safe-themes-original-value custom-safe-themes)

(defun theme-knockout--other-theme ()
  "Load the theme of the current matchup that is not currently loaded.

If the current theme is not in the current matchup, the theme in the car of the
current matchup will be loaded."
  (unless (theme-knockout--tournament-winnerp theme-knockout--theme-tournament)
    (let ((current-match (queue-first (theme-knockout-tournament-matches
                                       theme-knockout--theme-tournament))))
      (if (equal (car custom-enabled-themes)
                 (car current-match))
          (load-theme (cdr current-match) t)
        (load-theme (car current-match)) t))))

(defun theme-knockout--choose-theme ()
  "Mark the current theme as preferred over the other theme in current matchup."
  (unless (theme-knockout--tournament-winnerp theme-knockout--theme-tournament)
    (setf theme-knockout--theme-tournament
          (theme-knockout--tournament-next-match theme-knockout--theme-tournament
                                                 (car custom-enabled-themes)))
    (theme-knockout--other-theme)))

(defun theme-knockout--revert-theme ()
  "Revert theme to whatever it was before the tournament was started."
  (load-theme theme-knockout--original-theme t))

(defun theme-knockout--undo-choice ()
  "Undo the last selection of preferred theme."
  (unless (null (theme-knockout-tournament-match-log
                 theme-knockout--theme-tournament))
    (theme-knockout--tournament-undo-match theme-knockout--theme-tournament)
    (theme-knockout--other-theme)))

(declare-function theme-knockout--hydra/body "theme-knockout" nil t)

(defgroup theme-knockout ()
  "Customise group for theme-knockout."
  :group 'faces)

(defcustom theme-knockout-competitors (custom-available-themes)
  "Themes that should take part in the knockout tournament."
  :group 'theme-knockout
  :type '(repeat smybol))

(defcustom theme-knockout-show-theme-name nil
  "Themes that should take part in the knockout tournament."
  :group 'theme-knockout
  :type 'boolean)

(defun theme-knockout--make-hydra-docstring ()
  "Generate docstring for theme-knockout--hydra."
  (concat
   "​" ; zero width space to force empty line
   (when theme-knockout-show-theme-name
     (format "%s:\n" (car custom-enabled-themes)))
   "\n"
   "     "
   (theme-knockout--color-block bright-black)
   (theme-knockout--color-block red)
   (theme-knockout--color-block green)
   (theme-knockout--color-block yellow)
   (theme-knockout--color-block blue)
   (theme-knockout--color-block magenta)
   (theme-knockout--color-block cyan)
   (theme-knockout--color-block white)
   "     pack my box with five dozen liquor jugs"
   "     PACK MY BOX WITH FIVE DOZEN LIQUOR JUGS"
   "\n"
   "     "
   (theme-knockout--color-block black)
   (theme-knockout--color-block bright-red)
   (theme-knockout--color-block bright-green)
   (theme-knockout--color-block bright-yellow)
   (theme-knockout--color-block bright-blue)
   (theme-knockout--color-block bright-magenta)
   (theme-knockout--color-block bright-cyan)
   (theme-knockout--color-block bright-white)
   #("     pack my box with five dozen liqour jugs" 0 38 (face italic))
   #("     PACK MY BOX WITH FIVE DOZEN LIQUOR JUGS" 0 38 (face italic))
   "\n"
   "\nTheme knockout tournament"))

(defvar theme-knockout-last-tournament-log nil
  "Contains a log of the matches in the previous tuornament,

Once a tournament ends (prematurely or not), will be populated by the completed
matches in the form of a list of cons cells of (winner . loser) from least to
most recent.")

(defhydra theme-knockout--hydra
  (:pre (progn
          (when (theme-knockout--tournament-winnerp
                 theme-knockout--theme-tournament)
            (message (format "The winning theme is: %s"
                             (car custom-enabled-themes)))))
   :post (setf
          theme-knockout-last-tournament-log
          (reverse (theme-knockout-tournament-match-log
                    theme-knockout--theme-tournament))
          theme-knockout--theme-tournament nil
          custom-safe-themes theme-knockout--custom-safe-themes-original-value)

   :color pink
   :hint nil)
  ""
  ("<right>" (lambda () (interactive) (theme-knockout--other-theme))
   "other theme")
  ("<left>" (lambda () (interactive) (theme-knockout--other-theme))
   "other theme")
  ("<up>" (lambda () (interactive) (theme-knockout--undo-choice))
   "undo previous choice")
  ("<down>" (lambda () (interactive) (theme-knockout--choose-theme))
   "select theme")
  ("q" (lambda () (interactive) (theme-knockout--revert-theme))
   "quit (revert theme)" :color blue)
  ("Q" nil "quit" :color blue))

(setq theme-knockout--hydra/hint
      '(eval (hydra--format
              nil
              '(nil nil :hint nil)
              (theme-knockout--make-hydra-docstring)
              theme-knockout--hydra/heads)))

(defun theme-knockout ()
  "Start a knockout tournament to find your favourite theme.

Will launch a hydra with the following bindings:
<left/right>: view the other theme in the current matchup
<down>: mark current theme in matchup as preferred
q: quit, and restore the theme before the tournament was started
Q: quit, but keep the current theme.

WARNING: this will set `custom-safe-themes' to t while the hydra is active."
  (interactive)
  (setf theme-knockout--theme-tournament
        (theme-knockout--tournament-from-competitiors theme-knockout-competitors)
        theme-knockout--custom-safe-themes-original-value custom-safe-themes
        custom-safe-themes t
        theme-knockout--original-theme (car custom-enabled-themes))
  (theme-knockout--other-theme)
  (theme-knockout--hydra/body))

(provide 'theme-knockout)

;;; theme-knockout.el ends here
